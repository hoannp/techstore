@extends('layouts.app')
@section('content')
    <div class="text-center alert-404">
        <div><a href="{{route('home')}}" class="btn btn-light">Back to Home</a></div>
    </div>
@endsection
