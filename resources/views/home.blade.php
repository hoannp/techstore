@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"><h5>{{ __('Dashboard') }}</h5></div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <h5 class="text-center">You are logged in as {{auth()->user()->name}}</h5>
                </div>
            </div>
            <div class="card statistic-card">
                <div class="card-header">
                    <h5>System Statistic</h5>
                </div>
                <div class="card-body">
                    <table class="table table-dark text-center">
                        <tr>
                            <td>Categories: {{$categories}}</td>
                            <td>Products: {{$products}}</td>
                        </tr>
                        <tr>
                            <td>Roles: {{$roles}}</td>
                            <td>Permissions: {{$permissions}}</td>
                        </tr>
                        <tr>
                            <td colspan="2">Members: {{$users}}</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
