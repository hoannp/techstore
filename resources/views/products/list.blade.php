<table class="table table-striped">
    <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Name</th>
            <th scope="col">Price</th>
            <th scope="col">Description</th>
            <th scope="col">Category</th>
            <th scope="col">Action</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($products as $product)
            <tr>
                <td>{{ $product->id }}</td>
                <td>{{ $product->name }}</td>
                <td>{{ $product->price }}</td>
                <td>{{ $product->description }}</td>
                <td>{{ $product->categories->name }}</td>
                <td>
                    <button class="btn btn-dark" id="show-product" data-id="{{ $product->id }}">Detail</button>
                    <button class="btn btn-primary" id="edit-product" data-id="{{ $product->id }}">Edit</button>
                    <button class="btn btn-danger" id="delete-product" data-id="{{ $product->id }}">Delete</button>
                </td>
            </tr>
        @endforeach
    </tbody>
</table>
