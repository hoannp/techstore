<form class="modal-content" id="form-create-product" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="modal-header">
        <h5 class="modal-title">Create product</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
    </div>
    <div class="modal-body">
        <!-- Start Form Create Product Modal -->
        <div class="mb-3">
            <input type="text" class="form-control" name="name" placeholder="Name..." />
            <p class="text-danger" id="name-error"></p>
        </div>

        <div class="mb-3">
            <input type="number" step="any" class="form-control" name="price"
                placeholder="Price..." />
            <p class="text-danger" id="price-error"></p>
        </div>

        <div class="mb-3">
            <textarea class="form-control" rows="3" name="description"
                placeholder="Descriptions..."></textarea>
            <p class="text-danger" id="description-error"></p>
        </div>

        <div class="mb-3">
            <label class="form-label">Category</label>
            <select class="form-select" name="category_id">
                <option selected>Open this select category</option>
                @foreach ($categories as $category)
                    <option value="{{ $category->id }}">{{ $category->name }}</option>
                @endforeach
            </select>
        </div>

        <div class="mb-3">
            <label class="form-label">Thumbnail product</label>
            <input class="form-control" type="file" name="thumbnail" />
            <input type="hidden" id="id" />
            <p class="text-danger" id="thumbnail-error"></p>
        </div>
        <!-- End Form Create Product Modal -->
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary" id="btn-submit">Create</button>
    </div>
</form>
