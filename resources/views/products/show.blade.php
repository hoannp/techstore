 <!-- Start Card Product Modal -->
 <div class="card">
    <img src="{{  asset('storage/' . $product->thumbnail) }}" class="card-img-top" />
    <div class="card-body">
        <h5 class="card-title" id="name">Detail product</h5>
        <p class="card-text" id="description">{{ $product->description }}</p>
        <p class="text-success" id="price">{{ $product->price }}</p>
    </div>
</div>
<!-- End Card Product Modal -->
