@extends('layouts.app')

@section('content')
    <!-- Button Create Product Modal -->
    {{-- <button type="button" class="btn btn-primary" id="btn-create">
        Create product
    </button> --}}

    @can('view-products')
    <div class="widget">
        <button type="button" class="btn btn-primary" id="btn-create" data-bs-toggle="modal" data-bs-target="#form-create-modal">
            Create product
        </button>
        <!-- Button Show Product Modal -->
        <button type="button" class="d-none" id="btn-show" data-bs-toggle="modal" data-bs-target="#show-product">
            Show product
        </button>
    </div>
    @endcan
    <!-- Start Table Products -->
    <div id="table-list">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Name</th>
                    <th scope="col">Price</th>
                    <th scope="col">Description</th>
                    <th scope="col">Category</th>
                    <th scope="col">Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($products as $product)
                    <tr>
                        <td>{{ $product->id }}</td>
                        <td>{{ $product->name }}</td>
                        <td>{{ $product->price }}</td>
                        <td>{{ $product->description }}</td>
                        <td>{{ $product->categories->name }}</td>
                        <td>
                            <button class="btn btn-dark" id="show-product" data-id="{{ $product->id }}">Detail</button>
                            <button class="btn btn-primary" id="edit-product" data-id="{{ $product->id }}">Edit</button>
                            <button class="btn btn-danger" id="delete-product" data-id="{{ $product->id }}">Delete</button>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    {{ $products->links() }}
    <!-- End Table Products -->

    <!-- Start Create Product Modal -->
    <div class="modal fade" id="form-create-modal" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog"></div>
    </div>
    <!-- End Create Product Modal -->

    <!-- Start Update Product Modal -->
    <div class="modal fade" id="form-update-modal" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog">
            <form class="modal-content" id="form-update-product" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title">Update product</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <!-- Start Form Update Product Modal -->
                    <div class="mb-3">
                        <input type="text" class="form-control" id="name" name="name" />
                        <p class="text-danger" id="name-error"></p>
                    </div>

                    <div class="mb-3">
                        <input type="number" step="any" class="form-control" id="price" name="price" />
                        <p class="text-danger" id="price-error"></p>
                    </div>

                    <div class="mb-3">
                        <textarea class="form-control" rows="3" id="description" name="description"></textarea>
                        <p class="text-danger" id="description-error"></p>
                    </div>

                    <div class="mb-3">
                        <label class="form-label">Thumbnail product</label>
                        <input class="form-control" type="file" name="thumbnail" />
                        <input type="hidden" id="id" name="id" />
                        <p class="text-danger" id="thumbnail-error"></p>
                    </div>
                    <!-- End Form Update Product Modal -->
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary" id="btn-update">Update</button>
                </div>
            </form>
        </div>
    </div>
    <!-- End Create Product Modal -->

    <!-- Start Show Product Modal -->
    <div class="modal fade" id="show-product-modal" tabindex="-1" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Detail Product</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <!-- End Show Product Modal -->

    {{-- Start Delete Role Model --}}
    <div class="modal" id="delete-product-modal" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Delete product?</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <p>U want to delete product?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-dark" data-bs-dismiss="modal">No</button>
                    <button type="button" id="btn-delete" class="btn btn-danger">Delete</button>
                </div>
            </div>
        </div>
    </div>
    {{-- End Delete Role Model --}}
@endsection
@section('script')
    <script defer src="{{ asset('js/product-script.js') }}" type="text/javascript"></script>
@endsection
