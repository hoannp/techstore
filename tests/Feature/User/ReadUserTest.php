<?php

namespace Tests\Feature\User;

use App\Http\Traits\Routes\UserRoutes;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class ReadUserTest extends TestCase
{
    use UserRoutes;

    /** @test */
    public function unauthenticated_user_can_not_view_all_user()
    {
        $response = $this->get($this->getUserIndexRoute());
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect(route('login'));
    }

    /** @test */
    public function unauthenticated_user_can_not_view_a_specific_user()
    {
        $user = User::factory()->create();
        $user->roles()->attach($this->getRandomRole());
        $response = $this->get('/members/fetch');
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect(route('login'));
    }

    /** @test */
    public function admin_can_view_all_user()
    {
        $admin = User::factory()->create();
        $admin->roles()->attach($this->getAdminRole());
        $this->actingAs($admin);
        $user = User::factory()->create();
        $user->roles()->attach($this->getRandomRole());
        $response = $this->get($this->getUserIndexRoute());
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('members.index');
        $response->assertSee($user->role);
    }

    /** @test */
    public function non_admin_can_not_view_all_user()
    {
        $other = User::factory()->create();
        $other->roles()->attach($this->getMemberRole());
        $this->actingAs($other);
        $response = $this->get($this->getUserIndexRoute());
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('403');
    }

    /** @test */
    public function admin_can_view_a_specific_user()
    {
        $admin = User::factory()->create();
        $admin->roles()->attach($this->getAdminRole());
        $this->actingAs($admin);
        $user = User::factory()->create();
        $user->roles()->attach($this->getRandomRole());
        $response = $this->get($this->getUserIndexRoute());
        $response->assertStatus(Response::HTTP_OK);
        $response->assertSee($user->role)
            ->assertSee($user->name);
    }

    /** @test */
    public function non_admin_can_not_view_a_specific_user()
    {
        $other = User::factory()->create();
        $other->roles()->attach($this->getMemberRole());
        $this->actingAs($other);
        $user = User::factory()->create();
        $user->roles()->attach($this->getRandomRole());
        $response = $this->get('/members/fetch');
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('403');
    }


}
