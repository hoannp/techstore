<?php

namespace Tests\Feature\User;

use App\Http\Traits\Routes\UserRoutes;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class DeleteUserTest extends TestCase
{
    use UserRoutes;

    /** @test */
    public function unauthenticated_user_can_not_delete_user()
    {
        $user = User::factory()->create();
        $response = $this->delete($this->getUserDeleteRoute($user->id));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect(route('login'));
    }

    /** @test */
    public function non_admin_can_not_delete_user()
    {
        $other = User::factory()->create();
        $other->roles()->attach($this->getMemberRole());
        $this->actingAs($other);
        $user = User::factory()->create();
        $response = $this->delete($this->getUserDeleteRoute($user->id));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('403');
    }

    /** @test */
    public function admin_can_delete_user()
    {
        $admin = User::factory()->create();
        $admin->roles()->attach($this->getAdminRole());
        $this->actingAs($admin);
        $user = User::factory()->create();
        $response = $this->delete($this->getUserDeleteRoute($user->id));
        $response->assertStatus(Response::HTTP_OK);
        $this->assertDatabaseMissing('users', $user->toArray());
    }
}
