<?php

namespace Tests\Feature\User;

use App\Http\Traits\Routes\UserRoutes;
use App\Models\Role;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Illuminate\Support\Str;
use Tests\TestCase;

class UpdateUserTest extends TestCase
{
    use UserRoutes;

    /** @test */
    public function unauthenticated_user_can_not_view_edit_form()
    {
        $user = User::factory()->create();
        $response = $this->get('/members/fetch');
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect(route('login'));
    }

    /** @test */
    public function non_admin_can_not_view_edit_form()
    {
        $other = User::factory()->create();
        $other->roles()->attach($this->getMemberRole());
        $this->actingAs($other);
        $user = User::factory()->create();
        $response = $this->get('/members/fetch');
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('403');
    }

    /** @test */
    public function admin_can__view_edit_form()
    {
        $admin = User::factory()->create();
        $admin->roles()->attach($this->getAdminRole());
        $this->actingAs($admin);
        $user = User::factory()->create();
        $role = Role::factory()->create();
        $response = $this->get('/members/fetch');
        $response->assertSee($role->name);
    }

    /** @test */
    public function unauthenticated_user_can_not_update_user()
    {
        $user = User::factory()->create();
        $user->name = Str::random(5) . ' update';
        $response = $this->put($this->getUserUpdateRoute($user->id), $user->toArray());
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect(route('login'));
    }

    /** @test */
    public function non_admin_can_not_update_user()
    {
        $other = User::factory()->create();
        $other->roles()->attach($this->getMemberRole());
        $this->actingAs($other);
        $user = User::factory()->create();
        $user->name = Str::random(5) . ' update';
        $response = $this->put($this->getUserUpdateRoute($user->id), $user->toArray());
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('403');
    }

    /** @test */
    public function admin_can_update_user()
    {
        $admin = User::factory()->create();
        $admin->roles()->attach($this->getAdminRole());
        $this->actingAs($admin);
        $user = User::factory()->create()->makeVisible('password');
        $name = Str::random(5) . ' update';
        $user->name = $name;
        $user->password_confirmation = $this->getPassword();
        $response = $this->put($this->getUserUpdateRoute($user->id), $user->toArray());
        $this->assertDatabaseHas('users', ['id' => $user->id, 'name' => $name]);
    }

    /** @test */
    public function admin_can_not_update_user_if_name_null()
    {
        $admin = User::factory()->create();
        $admin->roles()->attach($this->getAdminRole());
        $this->actingAs($admin);
        $user = User::factory()->create()->makeVisible('password');
        $user->name = null;
        $user->password_confirmation = $this->getPassword();
        $response = $this->from('/members/fetch')
            ->put($this->getUserUpdateRoute($user->id), $user->toArray());
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/members/fetch');
        $response->assertSessionHasErrors('name');
    }

    /** @test */
    public function admin_can_not_update_user_if_email_already_existed()
    {
        $admin = User::factory()->create();
        $admin->roles()->attach($this->getAdminRole());
        $this->actingAs($admin);
        $email = $this->getExistedEmail();
        $user = User::factory()->create()->makeVisible('password');
        $user->email = $email;
        $user->password_confirmation = $this->getPassword();
        $response = $this->from('/members/fetch')
            ->put($this->getUserUpdateRoute($user->id), $user->toArray());
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/members/fetch');
        $response->assertSessionHasErrors('email');
    }

    /** @test */
    public function admin_can_update_user_if_email_already_existed_but_itself()
    {
        $admin = User::factory()->create();
        $admin->roles()->attach($this->getAdminRole());
        $this->actingAs($admin);
        $user = User::factory()->create()->makeVisible('password');
        $user->password_confirmation = $this->getPassword();
        $response = $this->put($this->getUserUpdateRoute($user->id), $user->toArray());
        $response->assertSessionHasNoErrors('email');
        $this->assertDatabaseHas('users', ['id' => $user->id, 'email' => $user->email]);
    }
}
