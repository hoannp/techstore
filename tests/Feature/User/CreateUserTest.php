<?php

namespace Tests\Feature\User;

use App\Http\Traits\Routes\UserRoutes;
use App\Models\Role;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class CreateUserTest extends TestCase
{
    use UserRoutes;

    /** @test */
    public function unauthenticated_user_can_not_view_create_form()
    {
        $response = $this->get('/members/fetch');
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect(route('login'));
    }

    /** @test */
    public function non_admin_can_not_view_create_form()
    {
        $other = User::factory()->create();
        $other->roles()->attach($this->getMemberRole());
        $this->actingAs($other);
        $response = $this->get('/members/fetch');
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('403');
    }

    /** @test */
    public function admin_can_view_create_form()
    {
        $admin = User::factory()->create();
        $admin->roles()->attach($this->getAdminRole());
        $this->actingAs($admin);
        $role = Role::factory()->create();
        $response = $this->get('/members/fetch');
        $response->assertSee($role->name);
    }

    /** @test */
    public function unauthenticated_user_can_not_create_user()
    {
        $user = User::factory()->make()->toArray();
        $response = $this->post($this->getUserStoreRoute(), $user);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect(route('login'));
    }

    /** @test */
    public function non_admin_can_not_create_user()
    {
        $other = User::factory()->create();
        $other->roles()->attach($this->getMemberRole());
        $this->actingAs($other);
        $user = User::factory()->make([
            'password_confirmation' => $this->getPassword()
        ])->makeVisible('password');
        $response = $this->post($this->getUserStoreRoute(), $user->toArray());
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('403');
    }

    /** @test */
    public function admin_can_create_user()
    {
        $admin = User::factory()->create();
        $admin->roles()->attach($this->getAdminRole());
        $this->actingAs($admin);
        $user = User::factory()->make([
            'password_confirmation' => $this->getPassword()
        ])->makeVisible('password');
        $response = $this->post($this->getUserStoreRoute(), $user->toArray());
        $this->assertDatabaseHas('users', ['email' => $user->email]);
    }

    /** @test */
    public function admin_can_not_create_user_if_name_null()
    {
        $admin = User::factory()->create();
        $admin->roles()->attach($this->getAdminRole());
        $this->actingAs($admin);
        $user = User::factory()->make([
            'name' => null,
            'password_confirmation' => $this->getPassword()
        ])->makeVisible('password');
        $response = $this->from('/members/fetch')
            ->post($this->getUserStoreRoute(), $user->toArray());
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors('name');
        $response->assertRedirect('/members/fetch');
    }

    /** @test */
    public function authenticated_user_can_not_create_user_if_email_existed()
    {
        $admin = User::factory()->create();
        $admin->roles()->attach($this->getAdminRole());
        $this->actingAs($admin);
        $user = User::factory()->make([
            'email' => $this->getExistedEmail(),
            'password_confirmation' => $this->getPassword()
        ])->makeVisible('password');
        $response = $this->from('/members/fetch')
            ->post($this->getUserStoreRoute(), $user->toArray());
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertSessionHasErrors('email');
        $response->assertRedirect('/members/fetch');
    }
}
