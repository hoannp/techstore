<?php

namespace Tests\Feature;

use App\Models\Product;
use App\Models\User;
use Illuminate\Http\Response;
use Tests\TestCase;

class ProductTest extends TestCase
{

    /** @test */
    public function admin_can_see_product_list()
    {
        $admin = User::factory()->create();
        $admin->roles()->attach($this->getAdminRole());
        $this->actingAs($admin);
        $user = User::factory()->create();
        $user->roles()->attach($this->getRandomRole());
        $response = $this->get($this->getProductRoute());
        $response->assertViewIs('products.index');
        $response->assertStatus(Response::HTTP_OK);
    }

    /** @test */
    public function user_can_not_see_product_list()
    {
        $member = User::factory()->create();
        $member->roles()->attach($this->getMemberRole());
        $this->actingAs($member);
        $response = $this->get($this->getProductRoute());
        $response->assertViewIs('403');
        $response->assertStatus(Response::HTTP_OK);
    }

    /** @test */
    public function unauthenticated_user_can_not_see_product_list()
    {
        $response = $this->get($this->getProductRoute());
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect(route('login'));
    }

    /** @test */
    public function unauthenticated_user_can_not_single_product()
    {
        $product = Product::factory()->create();
        $response = $this->get($this->getShowProductRoute($product->id));
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect(route('login'));
    }

    public function getProductRoute()
    {
        return route('products.index');
    }

    public function getShowProductRoute(int $id)
    {
        return route('products.show', $id);
    }
}
