<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Http\Response;
use Tests\TestCase;

class RoleTest extends TestCase
{
    /** @test */
    public function authenticated_user_can_see_role_list()
    {
        $this->actingAs(User::factory()->create());
        $response = $this->get($this->getRoleRoute());
        $response->assertViewIs('roles.index');
        $response->assertSee('admin');
        $response->assertStatus(Response::HTTP_OK);
    }

    /** @test */
    public function authenticated_user_can_read_single_role()
    {
        $this->actingAs(User::factory()->create());
        $response = $this->get($this->getShowRoleRoute(1));
        $response->assertViewIs('roles.show');
        $response->assertStatus(Response::HTTP_OK);
    }


    public function getRoleRoute()
    {
        return route('roles.index');
    }

    public function getShowRoleRoute(int $id)
    {
        return route('roles.show', $id);
    }
}
