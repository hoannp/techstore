<?php

namespace Tests\Feature;

use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Response;
use Tests\TestCase;

class DeleteRoleTest extends TestCase
{
    /** @test */
    public function authenticated_user_can_delete_role()
    {
        $this->actingAs(User::factory()->create());
        $role = Role::factory()->create()->toArray();
        $response = $this->delete(route('roles.delete', $role['id']), $role);
        $response->assertRedirect(route('roles.index'));
        $response->assertStatus(Response::HTTP_FOUND);
    }
}
