<?php

namespace Tests;

use App\Models\Role;
use App\Models\User;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    public function getAdminRole(){
        $role = Role::where('name','admin')->first();
        return $role->id;
    }

    public function getMemberRole(){
        $role = Role::where('name','member')->first();
        return $role->id;
    }

    public function getRandomRole(){
        $roles = Role::all();
        foreach ($roles as $role ){
            $data[] = $role->id;
        }
        $position = array_rand($data);
        return $data[$position];
    }

    public function getPassword(){
        return '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi';
    }

    public function getExistedEmail(){
        $arr = User::all();
        foreach ($arr as $user){
            $data[] = $user->email;
        }
        $position = array_rand($data);
        return $data[$position];
    }
}
