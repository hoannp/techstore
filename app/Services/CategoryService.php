<?php
namespace App\Services;
use App\Repositories\CategoryRepository;
use Illuminate\Http\Request;

class CategoryService
{
    protected $categoryRepository;

    public function __construct(CategoryRepository $categoryRepository)
    {
        $this->categoryRepository = $categoryRepository;
    }

    public function store(Request $request)
    {
        $this->categoryRepository->store(['name' => $request->input('name')]);
    }

    public function update(Request $request, $id)
    {
        $category = $this->categoryRepository->getItemById($id);
        $category->update(['name' => $request->input('name')]);
    }

    public function delete($id)
    {
        $this->categoryRepository->getItemById($id)->delete();
    }
}
