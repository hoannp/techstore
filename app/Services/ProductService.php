<?php
namespace App\Services;

use App\Repositories\ProductRepository;
use App\Http\Requests\CreateProductRequest;
use Illuminate\Http\Request;

class ProductService {
    protected $productRepository;

    public function __construct(ProductRepository $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    public function store(CreateProductRequest $request)
    {
        $product = [
            'name' => $request->input('name'),
            'price' => $request->input('price'),
            'description' => $request->input('description'),
            'category_id' => (int)$request->input('category_id')
        ];

        if($request->hasFile('thumbnail'))
        {
            $product['thumbnail'] = $this->saveFile($request, 'thumbnail');
        }

        $this->productRepository->store($product);
    }

    public function update(Request $request)
    {
        $id = (int)$request->input('id');
        $product = $this->productRepository->getItemById($id);

        $product->name = $request->input('name');
        $product->price = $request->input('price');
        $product->description = $request->input('description');

        if($request->hasFile('thumbnail'))
        {
            $path = 'storage/' . $product->thumbnail;
            $this->deleteFile($path);
            $product['thumbnail'] = $this->saveFile($request, 'thumbnail');
        }

        $product->save();
    }

    public function deleteProductById(int $id)
    {
        $product = $this->productRepository->getItemById($id);
        $path = 'storage/' . $product->thumbnail;
        $this->deleteFile($path);
        $product->delete();
    }

    private function saveFile($request, string $file)
    {
        $file = $request->file($file);
        $extension = $file->getClientOriginalExtension();
        $fileName = time() . '.' . $extension;
        $file->move('storage', $fileName);
        return $fileName;
    }

    private function deleteFile(string $path)
    {
        if(file_exists($path))
        {
            unlink($path);
        }
    }
}
