<?php

namespace App\Services;

use App\Http\Requests\CreateRoleRequest;
use App\Repositories\RoleRepository;
use Illuminate\Http\Request;


class RoleService
{
    protected $roleRepository;

    public function __construct(RoleRepository $roleRepository)
    {
        $this->roleRepository = $roleRepository;
    }

    public function getAll()
    {
        return $this->roleRepository->getAll();
    }

    public function store(CreateRoleRequest $request)
    {
        $roleName = $request->input('name');
        $permission = $request->input('permissions');

        $permissionsNumber = array_map(function($permission) {
            return (int)$permission;
        }, $permission);

        $role = $this->roleRepository->store(['name' => $roleName]);
        $this->roleRepository->onAttach($role, $permissionsNumber);
        return $role;
    }

    public function edit($id)
    {
        $role = $this->roleRepository->getItemById($id);
        $data =  $role->permissions->toArray();

        $permissions = array_map(function($permission) {
            return $permission['id'];
        }, $data);

        return ['role' => $role, 'permissions' => $permissions];
    }

    public function update(CreateRoleRequest $request, $id)
    {
        $role = $this->roleRepository->getItemById($id);
        $role->update([
            'name' => $request->input('name')
        ]);

        $permissions = $request->input('permissions');

        $permissionsNumber = array_map(function($permission) {
            return (int)$permission;
        }, $permissions);

        $this->roleRepository->onSync($role, $permissionsNumber);

        return $role;
    }

    public function delete($id)
    {
        $role = $this->roleRepository->getItemById($id);
        $this->roleRepository->onDetach($role, $id);
        $role->delete();
        return $role;
    }
}
