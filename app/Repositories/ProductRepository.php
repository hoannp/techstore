<?php
namespace App\Repositories;

use App\Models\Product;

class ProductRepository extends BaseRepository
{
    protected Product $product;

    public function __construct(Product $product)
    {
        parent::__construct();
        $this->product = $product;
    }

    public function model()
    {
        return Product::class;
    }

    public function getProductsWithCategory(string $category)
    {
        return $this->product->with($category)->latest('id')->paginate(15);
    }
}
