<?php

namespace App\Http\Requests;

use App\Rules\VnPhoneNumber;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:255',
            'password' => 'required|min:8|confirmed',
            'email' => [
                'required',
                'email',
                Rule::unique('users')->ignore($this->route('id'))
            ],
            'phone' => [
                'required',
                new VnPhoneNumber
            ]
        ];
    }
}
