<?php

namespace App\Http\Traits\Routes;

trait UserRoutes
{
    public function getUserIndexRoute()
    {
        return route('members.index');
    }

    public function getUserShowRoute($id)
    {
        return route('members.show', ['id' => $id]);
    }

    public function getUserCreateRoute()
    {
        return route('members.create');
    }

    public function getUserStoreRoute()
    {
        return route('members.store');
    }

    public function getUserEditRoute($id)
    {
        return route('members.edit', ['id' => $id]);
    }

    public function getUserUpdateRoute($id)
    {
        return route('members.update', ['id' => $id]);
    }

    public function getUserDeleteRoute($id)
    {
        return route('members.delete', ['id' => $id]);
    }
}
