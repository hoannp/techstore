<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Permission;
use App\Models\Product;
use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $role;
    protected $permission;
    protected $user;
    protected $product;
    protected $category;

    public function __construct(Role $role, User $user, Permission $permission, Product $product, Category $category)
    {
        $this->middleware('auth');
        $this->user = $user;
        $this->role = $role;
        $this->permission = $permission;
        $this->product = $product;
        $this->category = $category;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $users = $this->user->all()->count();
        $products = $this->product->all()->count();
        $roles = $this->role->all()->count();
        $categories = $this->category->all()->count();
        $permissions = $this->permission->all()->count();
        return view('home', compact('users', 'products', 'roles', 'categories', 'permissions'));
    }
}
