<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateRoleRequest;
use App\Models\Role;
use App\Repositories\RoleRepository;
use App\Services\RoleService;

class RoleController extends Controller
{
    protected $roleRepository;
    protected $roleService;
    public function __construct(RoleRepository $roleRepository, RoleService $roleService)
    {
        $this->roleRepository = $roleRepository;
        $this->roleService = $roleService;
    }

    public function index()
    {
        return view('roles.index');
    }

    public function getAllRole()
    {
        $roles = $this->roleRepository->getAll();
        return $this->viewRender($roles);
    }

    public function show(int $id)
    {
        $role = $this->roleRepository->getItemById($id);
        return $this->viewRender($role);
    }

    public function store(CreateRoleRequest $request)
    {
        $role = $this->roleService->store($request);
        return $this->viewRender($role);
    }

    public function edit(int $id)
    {
        $roleData = $this->roleService->edit($id);

        return $this->viewRender([
            'id' => $roleData['role']->id,
            'name' => $roleData['role']->name,
            'permissions' => $roleData['permissions']
        ]);
    }

    public function update(CreateRoleRequest $request, int $id)
    {
       $role = $this->roleService->update($request, $id);
       return $this->viewRender($role);
    }

    public function destroy(int $id)
    {
        $role = $this->roleService->delete($id);
        return $this->viewRender($role);
    }
}
