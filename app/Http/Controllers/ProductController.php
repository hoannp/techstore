<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateProductRequest;
use App\Models\Category;
use App\Repositories\ProductRepository;
use App\Services\ProductService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ProductController extends Controller
{
    protected Category $category;
    protected ProductRepository $productRepository;
    protected ProductService $productService;

    public function __construct(Category $category, ProductRepository $productRepository, ProductService $productService)
    {
        $this->category = $category;
        $this->productRepository = $productRepository;
        $this->productService = $productService;
    }

    public function index()
    {
        $products = $this->productRepository->getAllPagination(15);
        return view('products.index', ['products' => $products]);
    }

    public function refresh()
    {
        $products = $this->productRepository->getProductsWithCategory('categories');
        $view = view('products.list', ['products' => $products])->render();
        return $this->viewRender([
            'status' => Response::HTTP_OK,
            'view' => $view
        ]);
    }

    public function show(int $id)
    {
        $product = $this->productRepository->getItemById($id);
        $view = view('products.show', ['product' => $product])->render();
        return $this->viewRender([
            'product' => $product,
            'view' => $view
        ]);
    }

    public function create()
    {
        $categories = $this->category->all();
        $view = view('products.create', ['categories' => $categories])->render();

        return $this->viewRender([
            'status' => Response::HTTP_OK,
            'view' => $view
        ]);
    }

    public function store(CreateProductRequest $request)
    {
        $this->productService->store($request);
        return $this->refresh();
    }

    public function update(Request $request)
    {
        $this->productService->update($request);
        return $this->refresh();
    }

    public function destroy(int $id)
    {
        $this->productService->deleteProductById($id);
        return $this->refresh();
    }
}
