<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateCategoryRequest;
use App\Repositories\CategoryRepository;
use App\Services\CategoryService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class CategoryController extends Controller
{
    protected CategoryRepository $categoryRepository;
    protected CategoryService $categoryService;

    public function __construct(CategoryRepository $categoryRepository, CategoryService $categoryService)
    {
        $this->categoryRepository = $categoryRepository;
        $this->categoryService = $categoryService;
    }

    public function index()
    {
        return view('categories.index');
    }

    public function getCategories()
    {
        $categories = $this->categoryRepository->getAll();
        return $this->viewRender($categories);
    }

    public function show(int $id)
    {
        $category = $this->categoryRepository->getItemById($id);
        return $this->viewRender($category);
    }

    public function store(CreateCategoryRequest $request)
    {
        $this->categoryService->store($request);

        return $this->viewRender([
            'status' => Response::HTTP_OK,
            'message' => 'created category'
        ]);
    }

    public function update(Request $request, int $id)
    {
        $category = $this->categoryService->update($request, $id);
        return $this->viewRender($category);
    }

    public function destroy(int $id)
    {
        $this->categoryService->delete($id);
        return $this->viewRender([
            'status' => Response::HTTP_OK,
            'message' => 'deleted category'
        ]);
    }
}
