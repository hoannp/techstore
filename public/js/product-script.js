$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

// ---------- Show product -------------------
$(document).on('click', '#show-product', function() {
    let id = $(this).data('id');
    $.ajax({
        type: 'GET',
        url: `/products/${id}`,
        dataType: 'json',
        success: function(response) {
            $('#show-product-modal').find('.modal-body').html(response.view);
            $('#show-product-modal').modal('show');
        }
    })
});

// ---------- Create product -------------------
$(document).on('click', '#btn-create', function() {
    $.ajax({
        type: 'GET',
        url: `/products/create`,
        dataType: 'json',
        success: response => {
            if(response.status === 200) {
                $('#form-create-modal').find('.modal-dialog').html(response.view);
                $('#form-create-modal').modal('show');
            }
        }
    })
})

$(document).on('submit', '#form-create-product', function(e) {
    e.preventDefault();
    let form = $('#form-create-product');
    let data = new FormData(form[0]);
    $.ajax({
        type: 'POST',
        url: '/products',
        dataType: 'json',
        data,
        contentType: false,
        processData: false,
        success: response => {
            if(response.status === 200) {
                $('#form-create-modal').modal('hide');
                $('#table-list').html(response.view);
            }
        },
        error: e => {
            $('#name-error').text(e.responseJSON.errors['name'] ?? '');
            $('#price-error').text(e.responseJSON.errors['price'] ?? '');
            $('#description-error').text(e.responseJSON.errors['description'] ?? '');
            $('#thumbnail-error').text(e.responseJSON.errors['thumbnail'] ?? '');
        }
    })
});


// ---------- Clean data product -------------------
function cleanData()
{
    $('#name-error').text('');
    $('#price-error').text('');
    $('#description-error').text('');
    $('#thumbnail-error').text('');

    $('#name').val('');
    $('#price').val('');
    $('#description').val('');
}

$(document).on('click', '#edit-product', function(e) {
    let id = $(this).data('id');

    $('#form-update-modal').modal('show');

    $.ajax({
        type: "GET",
        dataType: 'json',
        url: `/products/${id}`,
        success: response => {
            $('#id').val(response.product.id);
            $('#name').val(response.product.name);
            $('#price').val(response.product.price);
            $('#description').val(response.product.description);
        }
    })
});

$(document).on('submit', '#form-update-product', function(e) {
    e.preventDefault();

    let form = $('#form-update-product');
    let data = new FormData(form[0]);

    $.ajax({
        type: 'POST',
        url: `/products/update`,
        data,
        contentType: false,
        processData: false,
        success: function(response) {
            if(response.status === 200) {
                $('#form-update-modal').modal('hide');
                $('#table-list').html(response.view);
            }
        },
        error: (e) => {
            $('#name-error').text(e.responseJSON.errors['name'] ?? '');
            $('#price-error').text(e.responseJSON.errors['price'] ?? '');
            $('#description-error').text(e.responseJSON.errors['description'] ?? '');
            $('#thumbnail-error').text(e.responseJSON.errors['thumbnail'] ?? '');
        }
    });
});


// ---------- Delete product -------------------
$(document).on('click', '#delete-product', function(e) {
    let id = $(this).data('id');
    $('#id').val(id);
    $('#delete-product-modal').modal('show');
});

$(document).on('click', '#btn-delete', function() {
    let id = +$('#id').val();
    $.ajax({
        type: 'DELETE',
        dataType: 'json',
        url: `/products/${id}`,
        success: function(response) {
            if(response.status === 200) {
                $('#table-list').html(response.view);
                $('#delete-product-modal').modal('hide');
            }
        },
        error: (e) => {
            console.log(e);
            $('body').html(e.responseText);
        }
    })
});
