$('#text-add-category').show();
$('#text-update-category').hide();
$('#add-btn').show();
$('#update-btn').hide();


// ---------- Get all role -------------------
function getAllCategory()
{
    $('#refresh').hide();
    $.ajax({
        type: "GET",
        dataType: 'json',
        url: '/categories/get-all',
        success: function(response) {
            let xhtml = '';
            $.each(response, (key, category) => {
                xhtml += '<tr>';
                xhtml += '<td>' + category.id + '</td>';
                xhtml += '<td>' + category.name + '</td>';
                xhtml += '<td>';
                xhtml += '<button class="btn btn-dark mr-2" onClick="onShowCategory('+ category.id +')" >Detail</button>';
                xhtml += '<button class="btn btn-primary mr-2" onClick="onEditCategory('+ category.id +')" >Edit</button>';
                xhtml += '<button class="btn btn-danger" onClick="onDeleteCategory('+ category.id +')">Delete</button>';
                xhtml += '</td>';
                xhtml += '</tr>';
            })
            $('tbody').html(xhtml);
        }
    })
}
getAllCategory();

// ---------- Reset data -------------------
function cleanData()
{
    $('#refresh').hide();
    $('#name').val('');
    $('#name-error').text('');
}

// ---------- Create category -------------------
function onAddCategory()
{
    let name = $('#name').val();
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: 'POST',
        data: { name },
        url: '/categories',
        success: function(response) {
            if(response.status === 200) {
                getAllCategory();
                cleanData();
            }
        },
        error: function(error) {
            $('#name-error').text(error.responseJSON.errors.name ?? '');
        }
    });
}

// ---------- Show category -------------------
function onShowCategory(id)
{
    cleanData();
    $.ajax({
        type: "GET",
        dataType: 'json',
        url: `/categories/${id}`,
        success: function(category) {
            $('#refresh').show();
            let xhtml = '<tr>';
            xhtml += '<td>' + category.id + '</td>';
            xhtml += '<td>' + category.name + '</td>';
            xhtml += '<td>';
            xhtml += '<button class="btn btn-primary mr-2" onClick="onEditCategory('+ category.id +')" >Edit</button>';
            xhtml += '<button class="btn btn-danger" onClick="onDeleteCategory('+ category.id +')">Delete</button>';
            xhtml += '</td>';
            xhtml += '</tr>';
            $('tbody').html(xhtml);
        }
    })
}

// ---------- Edit category -------------------
function onEditCategory(id)
{
    $('#add-btn').hide();
    $('#update-btn').show();
    $('#text-add-category').hide();
    $('#text-update-category').show();
    cleanData();
    $.ajax({
        type: "GET",
        dataType: 'json',
        url: `/categories/${id}`,
        success: function(category) {
            $('#id').val(category.id);
            $('#name').val(category.name);
        }
    })
}

// ---------- Update category -------------------
function onUpdateCategory()
{
    let id = $('#id').val();
    let name = $('#name').val();

    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: 'PUT',
        dataType: 'json',
        data: { id, name },
        url: `/categories/${id}`,
        success: function(response) {
            $('#add-role').show();
            $('#add-btn').show();
            $('#update-btn').hide();
            $('#update-role').hide();
            getAllCategory();
            cleanData();
        },
        error: function(error) {
            $('#name-error').text(error.responseJSON.errors.name ?? '');
            $('#permission-error').text(error.responseJSON.errors.permissions ?? '');
        }
    })
}


// ---------- Delete category -------------------
function onDeleteCategory(id)
{
    $('#delete-category-modal').modal('show');
    $('#add-category').show();
    $('#update-category').hide();
    $('#add-btn').show();
    $('#update-btn').hide();

    $('#btn-delete').on('click', () => {
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: 'DELETE',
            dataType: 'json',
            url: `/categories/${id}`,
            success: function(response) {
                getAllCategory();
                $('#delete-category-modal').modal('hide');
            }
        })
    })
}
