<?php

use App\Http\Controllers\CategoryController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\RoleController;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\MemberController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

// ---------- CRUD MEMBER ----------
Route::group([
    'prefix' => 'members',
    'middleware' => ['auth', 'role:admin']
], function () {

    Route::post('/create', [MemberController::class, 'store'])->name('members.store');

    Route::get('/fetch', [MemberController::class, 'fetch']);

    Route::put('/{id}/edit', [MemberController::class, 'update'])->name('members.update');

    Route::delete('/{id}/delete', [MemberController::class, 'delete'])->name('members.delete');

    Route::get('/', [MemberController::class, 'index'])->name('members.index');

    Route::get('/search', [MemberController::class, 'search'])->name('members.search');

});

// ---------- CRUD ROLE ----------
Route::group([
    'prefix' => 'roles',
    'middleware' => ['auth', 'role:admin']
], function () {
    Route::get('/', [RoleController::class, 'index'])
        ->name('roles.index');

    Route::get('/getAllRole', [RoleController::class, 'getAllRole'])
        ->name('roles.all');

    Route::get('/create', [RoleController::class, 'create'])
        ->name('roles.create');

    Route::post('/store', [RoleController::class, 'store'])
        ->name('roles.store');

    Route::get('/{id}/edit', [RoleController::class, 'edit'])
        ->name('roles.edit');

    Route::put('/update/{id}', [RoleController::class, 'update'])
        ->name('roles.update');

    Route::delete('/{id}', [RoleController::class, 'destroy'])
        ->name('roles.delete');

    Route::get('/{id}', [RoleController::class, 'show'])
        ->name('roles.show');
});


// ---------- CRUD PRODUCT ----------
Route::group([
    'prefix' => 'products',
    'middleware' => ['auth']
], function () {
    Route::get('/', [ProductController::class, 'index'])
        ->name('products.index')
        ->middleware('permission:product.view');

    Route::get('/create', [ProductController::class, 'create'])
        ->middleware(('permission:product.create'));

    Route::get('/get-products', [ProductController::class, 'getAllProduct'])
        ->middleware('permission:product.view');

    Route::post('/', [ProductController::class, 'store'])
        ->middleware('permission:product.create');

    Route::get('/{id}', [ProductController::class, 'show'])
        ->name('products.show')
        ->middleware('permission:product.view');

    Route::post('/update', [ProductController::class, 'update'])
        ->middleware('permission:product.update');
    Route::post('/{id}', [ProductController::class, 'update'])
        ->middleware('permission:product.update');

    Route::delete('/{id}', [ProductController::class, 'destroy'])
        ->middleware('permission:product.delete');
});

// ---------- CRUD CATEGORY ----------
Route::group([
    'prefix' => 'categories',
    'middleware' => ['auth']
], function () {
    Route::get('/', [CategoryController::class, 'index'])
        ->name('categories.index')
        ->middleware('permission:product.view');

    Route::get('/get-all', [CategoryController::class, 'getCategories']);

    Route::get('/{id}', [CategoryController::class, 'show']);

    Route::post('/', [CategoryController::class, 'store'])
        ->middleware('permission:product.create');

    Route::get('/{id}', [CategoryController::class, 'show'])
        ->middleware('permission:product.view');

    Route::put('/{id}', [CategoryController::class, 'update'])
        ->middleware('permission:product.update');

    Route::delete('/{id}', [CategoryController::class, 'destroy'])
        ->middleware('permission:product.delete');
});
